import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Belajar Form Flutter",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("BelajarFlutter.com"),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: [
              TextFormField(
                autofocus: true,
                decoration: new InputDecoration(
                    hintText: "Tegar karunia ilham",
                    labelText: "Nama Lengkap",
                    icon: Icon(Icons.people)),
              ),

              TextFormField(
                keyboardType: TextInputType.phone,
                obscureText: true,
                autofocus: true,
                decoration: new InputDecoration(
                  hintText: "085xxxxxxxxx",
                  labelText: "Nomer Telp",
                  icon: Icon(Icons.phone),
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                ),
                validator: (value) {
                  if (value!.isEmpty) {}
                  return 'Password Tidak Boleh kosong';
                },
              ),
              RaisedButton(
                child: Text(
                  "submit",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.blue,
                onPressed: () {
                  if (_formKey.currentState!.validate()) {}
                },
              ),
              // tambahkan komponen seperti input field disini
            ],
          ),
        ),
      ),
    );
  }
}
