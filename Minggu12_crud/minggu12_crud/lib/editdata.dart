import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import './main.dart';

class EditData extends StatefulWidget {
  final List list;
  final int index;

  EditData({required this.list, required this.index, id});
  @override
  _EditDataState createState() => new _EditDataState();
}

class _EditDataState extends State<EditData> {
  late TextEditingController controllerNama;
  late TextEditingController controllerUsername;
  late TextEditingController controllerEmail;
  late TextEditingController controllerPassword;
  late TextEditingController controllerIdakses;

  void editData() async {
    http.put(
        Uri.parse('http://192.168.100.169:8000/api/userApi/' +
            widget.list[widget.index]['id'].toString()),
        headers: {
          'Accept': 'application/json',
        },
        body: {
          "name": controllerNama.text,
          "username": controllerUsername.text,
          "email": controllerEmail.text,
          "password": controllerPassword.text,
          "id_akses": controllerIdakses.text,
        }).then((response) {
      print('Response status:${response.statusCode}');
      print('Response body ${response.body}');
    });
  }

  @override
  void initState() {
    controllerNama = new TextEditingController(
        text: widget.list[widget.index]['name'].toString());
    controllerUsername = new TextEditingController(
        text: widget.list[widget.index]['username'].toString());
    controllerEmail = new TextEditingController(
        text: widget.list[widget.index]['email'].toString());
    controllerPassword = new TextEditingController(
        text: widget.list[widget.index]['password'].toString());
    controllerIdakses = new TextEditingController(
        text: widget.list[widget.index]['id_akses'].toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("EDIT DATA"),
      ), // AppBar
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: ListView(
          children: <Widget>[
            new Column(
              children: <Widget>[
                new TextField(
                  controller: controllerNama,
                  decoration:
                      new InputDecoration(hintText: "Nama", labelText: "Nama"),
                ), // TextField
                new TextField(
                  controller: controllerUsername,
                  decoration: new InputDecoration(
                      hintText: "Username", labelText: "Username"), // InputDe
                ),
                new TextField(
                  controller: controllerEmail,
                  decoration: new InputDecoration(
                      hintText: "Email", labelText: "Email"), // Inp
                ), // TextField
                new TextField(
                  controller: controllerPassword,
                  obscureText: true,
                  decoration: new InputDecoration(
                      hintText: "Password", labelText: "Password"), //
                ), // TextField
                new TextField(
                  controller: controllerIdakses,
                  decoration: new InputDecoration(
                      hintText: "ID Akses", labelText: "ID Akses"), //
                ), // TextField
                new Padding(
                  padding: const EdgeInsets.all(10.0),
                ), // Padding
                new RaisedButton(
                  child: new Text("EDIT DATA"),
                  color: Colors.blueAccent,
                  onPressed: () {
                    editData();
                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (BuildContext context) => new Home())); // Mat
                  },
                )
              ], // <Widget>[]
            ), // Column
          ], // <widget>[]
        ), // ListView
      ),
    ); // Scaffold
  }
}
