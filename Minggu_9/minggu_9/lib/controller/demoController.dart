import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get_storage/get_storage.dart';
import '/model/product.dart';
import 'package:get/get.dart';

class DemoController extends GetxController {
  var CartItems = <Product>[].obs;
  int get cartCount => CartItems.length;
  double get totalAmount =>
      CartItems.fold(0.0, (sum, element) => sum + element.price);

  addToCart(Product product) {
    CartItems.add(product);
  }

  final storage = GetStorage();
  bool get isDart => storage.read('Dartmode') ?? false;
  ThemeData get theme => isDart ? ThemeData.dark() : ThemeData.light();
  void changeTheme(bool val) => storage.write('dartmode', val);
}
