import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Belajar Form Flutter",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("BelajarFlutter.com"),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: [
              // contoh checkbox
              Checkbox(
                value: true,
                onChanged: (value) {},
              ),

// contoh Switch
              Switch(
                value: true,
                onChanged: (value) {},
              ),

              CheckboxListTile(
                title: Text('Belajar Dasar Flutter'),
                subtitle: Text('Dart, widget, http'),
                value: true,
                activeColor: Colors.deepPurpleAccent,
                onChanged: (value) {},
              ),

              SwitchListTile(
                title: Text('Backend Programming'),
                subtitle: Text('Dart, Nodejs, PHP, Java, dll'),
                value: true,
                activeTrackColor: Colors.pink[100],
                activeColor: Colors.red,
                onChanged: (value) {},
              ),

              Slider(
                value: 25,
                min: 0,
                max: 100,
                onChanged: (value) {},
              ),

              // tambahkan komponen seperti input field disini
            ],
          ),
        ),
      ),
    );
  }
}
